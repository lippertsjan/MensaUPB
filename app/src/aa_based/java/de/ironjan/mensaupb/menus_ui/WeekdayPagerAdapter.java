package de.ironjan.mensaupb.unsorted;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


public class WeekdayPagerAdapter extends FragmentStatePagerAdapter {

    private final MenuListingFragment[] fragments = new MenuListingFragment[WeekdayHelper.DISPLAYED_DAYS_COUNT];
    private final String mRestaurant;

    private final WeekdayHelper mWeekdayHelperAA;

    public WeekdayPagerAdapter(Context context, FragmentManager fm, String restaurant) {
        super(fm);
        mWeekdayHelperAA = new WeekdayHelper(context, new CalendarDateProvider());
        mRestaurant = restaurant;
    }


    @Override
    public Fragment getItem(int i) {
        return getMenuListingFragment(i);
    }

    private Fragment getMenuListingFragment(int i) {
        if (fragments[i] == null) {
            String nextWeekDayAsKey = mWeekdayHelperAA.getNextWeekDayAsKey(i);
            MenuListingFragment fragment = MenuListingFragment.getInstance(nextWeekDayAsKey, mRestaurant);
            fragments[i] = fragment;
        }

        return fragments[i];
    }


    @Override
    public int getCount() {
        return WeekdayHelper.DISPLAYED_DAYS_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mWeekdayHelperAA.getNextWeekDayForUI(position);
    }

    public void onRefresh(){
        for(MenuListingFragment f: fragments) {
            if(f != null) {
                f.onRefresh();
            }
        }
    }
}