package de.ironjan.mensaupb.unsorted;

import de.ironjan.mensaupb.api_sort_into_correct_package_structure.model.Menu;

/**
 * Interface to handle navigation from a menu listing
 */
public interface MenusNavigationCallback {

    void showMenu(Menu m);
}
