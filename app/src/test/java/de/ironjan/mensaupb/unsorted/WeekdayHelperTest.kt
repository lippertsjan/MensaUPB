package de.ironjan.mensaupb.unsorted

import android.content.Context
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.util.Calendar
import java.util.Date


//@RunWith(MockitoJUnitRunner::class)
class WeekdayHelperTest() {
    @Mock
    private lateinit var context: Context

    @Mock
    private lateinit var dateProvider: DateProvider

    private lateinit var testee: WeekdayHelper

    @BeforeEach
    fun before() {
        MockitoAnnotations.openMocks(this)
        testee = WeekdayHelper(context, dateProvider)
    }


    @ParameterizedTest
    @CsvSource(
        // 2023-12-04 was a monday.

        "4, 0, ${Calendar.MONDAY}",
        "4, 7, ${Calendar.MONDAY}",
        "5, 0, ${Calendar.TUESDAY}",
        "6, 0, ${Calendar.WEDNESDAY}",
        "7, 0, ${Calendar.THURSDAY}",

        "8, 0, ${Calendar.FRIDAY}",

        // Weekends are skipped
        "7, 5, ${Calendar.THURSDAY}",
        "8, 5, ${Calendar.FRIDAY}",

        "5, 4, ${Calendar.MONDAY}",
        "5, 5, ${Calendar.TUESDAY}",
        "5, 6, ${Calendar.WEDNESDAY}",
        "5, 7, ${Calendar.THURSDAY}",

        "6, 3, ${Calendar.MONDAY}",

        "7, 2, ${Calendar.MONDAY}",

        "8, 1, ${Calendar.MONDAY}",

        "9, 0, ${Calendar.MONDAY}",
    )

    fun should_all_be_monday(day: Int, offset: Int, expected: Int){
        givenDayInDecember2023(day)

        val nextWeekDay = testee.getNextWeekDayWithOffset( offset)

        assertThatIsWeekday(expected, nextWeekDay)
    }


    private fun givenDayInDecember2023(day: Int) {
        val calendar = Calendar.getInstance()
        calendar.clear()
        calendar.set(2023, Calendar.DECEMBER, day, 15, 32, 7)

        `when`(dateProvider.today()).thenReturn(calendar.time)
    }

    private fun assertThatIsWeekday(expected: Int, nextWeekDay: Date?) {
        val calendar = Calendar.getInstance()
        calendar.time = nextWeekDay!!
        val actual = calendar.get(Calendar.DAY_OF_WEEK)

        assertEquals(expected, actual)
    }
}