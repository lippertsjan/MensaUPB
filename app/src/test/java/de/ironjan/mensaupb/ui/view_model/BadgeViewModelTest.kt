package de.ironjan.mensaupb.ui.view_model

import de.ironjan.mensaupb.R
import de.ironjan.mensaupb.domain.Badge
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class BadgeViewModelTest {
    @ParameterizedTest
    @MethodSource("provideKnownBadges")
    fun should_convert_known_badge(input: String, expected: BadgeViewModel) {
        val actual = BadgeViewModel.from(Badge(input))

        Assertions.assertThat(actual).usingRecursiveComparison().isEqualTo(expected)
    }

    @Test
    fun should_convert_unknown_badge() {
        val expected = BadgeViewModel(UNKNOWN_BADGE, null)

        val actual = BadgeViewModel.from(Badge(UNKNOWN_BADGE))

        Assertions.assertThat(actual).isEqualTo(expected)
    }


    companion object {
        private const val UNKNOWN_BADGE = "unknown"

        @JvmStatic
        fun provideKnownBadges(): Stream<Arguments> = Stream.of(
            Arguments.of(Badge.KEY_LOW_CALORIE, BadgeViewModel.LOW_CALORIE),
            Arguments.of(
                Badge.KEY_FAT_FREE,
                BadgeViewModel(Badge.KEY_FAT_FREE, R.string.fatFree)
            ),
            Arguments.of(
                Badge.KEY_VEGETARIAN,
                BadgeViewModel(Badge.KEY_VEGETARIAN, R.string.vegetarian)
            ),
            Arguments.of(
                Badge.KEY_VEGAN,
                BadgeViewModel(Badge.KEY_VEGAN, R.string.vegan)
            ),
            Arguments.of(
                Badge.KEY_NO_LACTOSE,
                BadgeViewModel(Badge.KEY_NO_LACTOSE, R.string.noLactose)
            ),
            Arguments.of(
                Badge.KEY_NO_GLUTEN,
                BadgeViewModel(Badge.KEY_NO_GLUTEN, R.string.noGluten)
            )
        )

    }
}