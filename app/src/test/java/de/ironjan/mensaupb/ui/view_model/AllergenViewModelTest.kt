package de.ironjan.mensaupb.ui.view_model

import de.ironjan.mensaupb.domain.Allergen
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream
import kotlin.test.Test


class AllergenViewModelTest {
    @ParameterizedTest
    @MethodSource("provideKnownAllergens")
    fun should_convert_known_allergens(input: String, expected: AllergenViewModel) {
        val actual = AllergenViewModel.from(Allergen(input))

        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun should_convert_unknown_allergen() {
        val expected = AllergenViewModel(UNKNOWN_ALLERGEN, null)

        val actual = AllergenViewModel.from(Allergen(UNKNOWN_ALLERGEN))

        assertThat(actual).isEqualTo(expected)
    }

    companion object {
        private const val UNKNOWN_ALLERGEN = "unknown"

        @JvmStatic
        fun provideKnownAllergens(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(Allergen.KEY_COLORED, AllergenViewModel.COLORED),
                Arguments.of(Allergen.KEY_CONSERVED, AllergenViewModel.CONSERVED),
                Arguments.of(Allergen.KEY_ANTIOXIDANTS, AllergenViewModel.ANTIOXIDANTS),
                Arguments.of(Allergen.KEY_FLAVOR_ENHANCERS, AllergenViewModel.FLAVOR_ENHANCERS),
                Arguments.of(Allergen.KEY_PHOSPHAT, AllergenViewModel.PHOSPHAT),
                Arguments.of(Allergen.KEY_SULFURATED, AllergenViewModel.SULFURATED),
                Arguments.of(Allergen.KEY_WAXED, AllergenViewModel.WAXED),
                Arguments.of(Allergen.KEY_BLACKENED, AllergenViewModel.BLACKENED),
                Arguments.of(Allergen.KEY_SWEETENER, AllergenViewModel.SWEETENER),
                Arguments.of(Allergen.KEY_PHENYLALANINE, AllergenViewModel.PHENYLALANINE),
                Arguments.of(Allergen.KEY_TAURINE, AllergenViewModel.TAURINE),
                Arguments.of(Allergen.KEY_NITRATE_SALT, AllergenViewModel.NITRATE_SALT),
                Arguments.of(Allergen.KEY_COFFEINE, AllergenViewModel.COFFEINE),
                Arguments.of(Allergen.KEY_QUININE, AllergenViewModel.QUININE),
                Arguments.of(Allergen.KEY_LACTOPROTEIN, AllergenViewModel.LACTOPROTEIN),
                Arguments.of(Allergen.KEY_CRUSTACEAN, AllergenViewModel.CRUSTACEAN),
                Arguments.of(Allergen.KEY_EGGS, AllergenViewModel.EGGS),
                Arguments.of(Allergen.KEY_FISH, AllergenViewModel.FISH),
                Arguments.of(Allergen.KEY_SOYA, AllergenViewModel.SOYA),
                Arguments.of(Allergen.KEY_LACTOSE, AllergenViewModel.LACTOSE),
                Arguments.of(Allergen.KEY_NUTS, AllergenViewModel.NUTS),
                Arguments.of(Allergen.KEY_CELERIAC, AllergenViewModel.CELERIAC),
                Arguments.of(Allergen.KEY_MUSTARD, AllergenViewModel.MUSTARD),
                Arguments.of(Allergen.KEY_SESAME, AllergenViewModel.SESAME),
                Arguments.of(Allergen.KEY_SULFITES, AllergenViewModel.SULFITES),
                Arguments.of(Allergen.KEY_MOLLUSKS, AllergenViewModel.MOLLUSKS),
                Arguments.of(Allergen.KEY_LUPINE, AllergenViewModel.LUPINE),
                Arguments.of(Allergen.KEY_GLUTEN, AllergenViewModel.GLUTEN),
                Arguments.of(Allergen.KEY_PEANUTS, AllergenViewModel.PEANUTS),
            )
        }
    }
}