package de.ironjan.mensaupb.ui.view_model

import de.ironjan.mensaupb.domain.Restaurant
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream


class RestaurantViewModelTest {
    @ParameterizedTest
    @MethodSource("provideKnownRestaurants")
    fun should_convert_known_restaurants(key: String, expected: RestaurantViewModel) {
        val actual = RestaurantViewModel.from(Restaurant(key))

        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun should_convert_unknown_restaurant() {
        val actual = RestaurantViewModel.from(Restaurant(UNKNOWN_RESTAURANT))

        assertThat(actual).isEqualTo(RestaurantViewModel(UNKNOWN_RESTAURANT, null))
    }

    companion object {
        private const val UNKNOWN_RESTAURANT = "unknown restaurant"

        @JvmStatic
        fun provideKnownRestaurants(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(Restaurant.KEY_MENSA_ACADEMICA_PADERBORN, RestaurantViewModel.MENSA_ACADEMICA),
                Arguments.of(Restaurant.KEY_MENSA_FORUM_PADERBORN, RestaurantViewModel.MENSA_FORUM),
                Arguments.of(Restaurant.KEY_BISTRO_HOTSPOT, RestaurantViewModel.BISTRO_HOTSPOT),
                Arguments.of(Restaurant.KEY_GRILL_CAFE, RestaurantViewModel.GRILL_CAFE),
                Arguments.of(Restaurant.KEY_CAFETE, RestaurantViewModel.CAFETE)
            )
        }
    }
}