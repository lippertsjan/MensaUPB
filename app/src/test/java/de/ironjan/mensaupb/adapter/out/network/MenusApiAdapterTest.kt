package de.ironjan.mensaupb.adapter.out.network

import de.ironjan.mensaupb.domain.Allergen
import de.ironjan.mensaupb.domain.Badge
import de.ironjan.mensaupb.domain.Menu
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import org.mockito.kotlin.whenever
import java.text.SimpleDateFormat

private const val DATE_STRING = "2024-04-30"

class MenusApiAdapterTest {
    private lateinit var testee: MenusApiAdapter

    val EXAMPLE_RESPONSE: String = """[
    {
        "date": "2024-04-30",
        "name_de": "Flammkuchen mit Tomate, Rucola,Parmesan",
        "name_en": "Tarte Flambee with tomato, rucola and parmesan",
        "description_de": "",
        "description_en": "",
        "category": "dish",
        "category_de": "Essen",
        "category_en": "Dish",
        "subcategory_de": "",
        "subcategory_en": "",
        "priceStudents": 3.7,
        "priceWorkers": 5.45,
        "priceGuests": 5.45,
        "allergens": [
            "15",
            "A1",
            "A7",
            "unknown allergen"
        ],
        "order_info": 49,
        "badges": ["low-calorie", "nonfat", "vegetarian", "unknown badge"],
        "restaurant": "grill-cafe",
        "pricetype": "fixed",
        "image": "http://www.studentenwerk-pb.de/fileadmin/imports/images/speiseleitsystem/7624.jpg",
        "key": "2024-04-30_grill-cafe_49"
    }
]"""


    val restClient = mock<RestClient>()  // unused mock usage NOT detected

    @BeforeEach
    fun setup() {
        testee = MenusApiAdapter(restClient)
    }

    @Test
    fun shouldBeEmpty() {
        whenApiResponseIs("[]")

        val actual = testee.getMenus()

        assertThat(actual).isEmpty()
    }

    private val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")

    @Test
    fun shouldContainExampleData() {
        val expected = listOf(
            Menu(
                simpleDateFormat.parse(DATE_STRING)!!,
                "Flammkuchen mit Tomate, Rucola,Parmesan",
                "Tarte Flambee with tomato, rucola and parmesan",
                "",
                "",
                "dish",
                "Essen",
                "Dish",
                "",
                "",
                3.7,
                5.45,
                5.45,
                listOf(
                    Allergen(Allergen.KEY_LACTOPROTEIN),
                    Allergen(Allergen.KEY_GLUTEN),
                    Allergen(Allergen.KEY_LACTOSE),
                    Allergen("unknown allergen")
                ),
                49,
                listOf(
                    Badge(Badge.KEY_LOW_CALORIE),
                    Badge(Badge.KEY_FAT_FREE),
                    Badge(Badge.KEY_VEGETARIAN),
                    Badge("unknown badge")
                ),
                "grill-cafe",
                "fixed",
                "http://www.studentenwerk-pb.de/fileadmin/imports/images/speiseleitsystem/7624.jpg",
                "2024-04-30_grill-cafe_49"
            )
        )

        whenApiResponseIs(EXAMPLE_RESPONSE)

        val actual = testee.getMenus()

        assertThat(actual)
            .usingRecursiveComparison()
            .isEqualTo(expected)
    }

    private fun whenApiResponseIs(exampleResponse: String) {
        whenever(restClient.get(MenusApiAdapter.MENUS_PATH)).thenReturn(exampleResponse)
    }
}