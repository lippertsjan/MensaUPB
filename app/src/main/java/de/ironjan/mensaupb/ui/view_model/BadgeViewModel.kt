package de.ironjan.mensaupb.ui.view_model

import de.ironjan.mensaupb.R
import de.ironjan.mensaupb.domain.Badge
import de.ironjan.mensaupb.domain.Badge.Companion.KEY_FAT_FREE
import de.ironjan.mensaupb.domain.Badge.Companion.KEY_LOW_CALORIE
import de.ironjan.mensaupb.domain.Badge.Companion.KEY_NO_GLUTEN
import de.ironjan.mensaupb.domain.Badge.Companion.KEY_NO_LACTOSE
import de.ironjan.mensaupb.domain.Badge.Companion.KEY_VEGAN
import de.ironjan.mensaupb.domain.Badge.Companion.KEY_VEGETARIAN

data class BadgeViewModel(
    val key: String,
    val stringId: Int?
) {

    companion object {

        val LOW_CALORIE = BadgeViewModel(KEY_LOW_CALORIE, R.string.lowCalorie)
        val FAT_FREE = BadgeViewModel(KEY_FAT_FREE, R.string.fatFree)
        val VEGETARIAN = BadgeViewModel(KEY_VEGETARIAN, R.string.vegetarian)
        val VEGAN = BadgeViewModel(KEY_VEGAN, R.string.vegan)
        val LACTOSE_FREE = BadgeViewModel(KEY_NO_LACTOSE, R.string.noLactose)
        val GLUTEN_FREE = BadgeViewModel(KEY_NO_GLUTEN, R.string.noGluten)

        fun from(badge: Badge): BadgeViewModel {
            return when (badge.key) {
                KEY_LOW_CALORIE -> LOW_CALORIE
                KEY_FAT_FREE -> FAT_FREE
                KEY_VEGETARIAN -> VEGETARIAN
                KEY_VEGAN -> VEGAN
                KEY_NO_LACTOSE -> LACTOSE_FREE
                KEY_NO_GLUTEN -> GLUTEN_FREE
                else -> BadgeViewModel(badge.key, null)
            }
        }
    }
}