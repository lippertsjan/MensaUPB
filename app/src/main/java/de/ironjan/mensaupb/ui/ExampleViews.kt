package de.ironjan.mensaupb.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.ironjan.mensaupb.ui.theme.MensaUPBTheme
import de.ironjan.mensaupb.ui.view_model.AllergenViewModel
import de.ironjan.mensaupb.ui.view_model.BadgeViewModel
import de.ironjan.mensaupb.ui.view_model.MenuListItemViewModel
import de.ironjan.mensaupb.ui.view_model.RestaurantViewModel

class ExampleViews {

    @Composable
    @Preview(showBackground = true)
    fun PreviewKnownRestaurant() {
        MensaUPBTheme {
            Restaurant(RestaurantViewModel.BISTRO_HOTSPOT)
        }
    }

    @Composable
    @Preview(showBackground = true)
    fun PreviewUnknownRestaurant() {
        MensaUPBTheme {
            Restaurant(RestaurantViewModel("some key", null))
        }
    }

    @Composable
    fun Restaurant(restaurantViewModel: RestaurantViewModel) {
        // TODO: move to vm ?
        val text = if (restaurantViewModel.stringId != null) {
            stringResource(restaurantViewModel.stringId)
        } else {
            restaurantViewModel.key
        }

        Surface(
            modifier = Modifier
                .fillMaxWidth()
                .shadow(8.dp)
        ) {
            Column(modifier = Modifier.padding(8.dp)) {
                Text(
                    text, style = MaterialTheme.typography.headlineSmall
                )
            }
        }
    }

    @Composable
    @Preview(showBackground = true)
    fun ExampleMenuListing() {
        MensaUPBTheme {
            MenusList(
                menus = ExampleMenus()
            )
        }
    }

    @Composable
    fun MenusList(menus: List<MenuListItemViewModel>) {
        LazyColumn {
            items(menus) { m ->
                MenuListView(menu = m)
            }
        }
    }

    @Composable
    fun MenuListView(menu: MenuListItemViewModel) {
        Surface(
            modifier = Modifier
                .fillMaxWidth()
                .shadow(8.dp)
        ) {
            Column(modifier = Modifier.padding(8.dp)) {
                Row {
                    // todo use subhead style
                    Text(
                        menu.name, style = MaterialTheme.typography.headlineSmall
                    )
                    Spacer(modifier = Modifier.weight(1f))
                    // TODO: add second composable for full text
                    // TODO: sort...
                    AllergenKeys(menu)
                }
                Spacer(modifier = Modifier.height(8.dp))

                Row {
                    Text(
                        String.format("%.2f€", menu.price), style = MaterialTheme.typography.bodySmall
                    )
                    Spacer(modifier = Modifier.weight(1f))
                    // TODO: extract to composable
                    // TODO: commata
                    menu.badges.forEach {
                        val badgeText = if (it.stringId != null) {
                            stringResource(it.stringId)
                        } else {
                            it.key
                        }
                        Text(
                            badgeText, style = MaterialTheme.typography.bodySmall
                        )
                    }
                }
            }
        }
    }


    @Composable
    private fun AllergenKeys(menu: MenuListItemViewModel) {
        Text(menu.allergens.map { it.key }.sorted().joinToString { it }, style = MaterialTheme.typography.bodySmall)
    }

    @Preview
    @Composable
    fun AllergenListPreview() {
        AllergenList(menu = menuListItemViewModelWithAllergensAndBadges())
    }

    private fun ExampleMenus(): List<MenuListItemViewModel> {
        return listOf(
            MenuListItemViewModel("Gericht 1", 1.7, listOf(), listOf()),
            MenuListItemViewModel("Gericht 2", 2.3, listOf(BadgeViewModel.VEGAN), listOf()),
            MenuListItemViewModel("Gericht 3", 2.3, badgeViewModels(), listOf()),
            menuListItemViewModelWithAllergensAndBadges()
        )
    }

    private fun menuListItemViewModelWithAllergensAndBadges() = MenuListItemViewModel(
        "Gericht 4",
        2.3,
        badgeViewModels(),
        allergenViewModels()
    )

    private fun badgeViewModels() = listOf(BadgeViewModel("unknown badge", null))

    private fun allergenViewModels() = listOf(AllergenViewModel.EGGS, AllergenViewModel.ANTIOXIDANTS, AllergenViewModel("XY", null))

    @Composable
    private fun AllergenList(menu: MenuListItemViewModel) {
        val joinToString = menu.allergens
            .map { if (it.stringId != null) stringResource(id = it.stringId) else it.key }
            .sorted()
            .joinToString { it }

        Text(joinToString)
    }
}