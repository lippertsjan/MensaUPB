package de.ironjan.mensaupb.ui.view_model

import de.ironjan.mensaupb.R
import de.ironjan.mensaupb.domain.Restaurant
import de.ironjan.mensaupb.domain.Restaurant.Companion.KEY_BISTRO_HOTSPOT
import de.ironjan.mensaupb.domain.Restaurant.Companion.KEY_CAFETE
import de.ironjan.mensaupb.domain.Restaurant.Companion.KEY_GRILL_CAFE
import de.ironjan.mensaupb.domain.Restaurant.Companion.KEY_MENSA_ACADEMICA_PADERBORN
import de.ironjan.mensaupb.domain.Restaurant.Companion.KEY_MENSA_FORUM_PADERBORN

data class RestaurantViewModel(val key: String, val stringId: Int?) {
    companion object {
        val MENSA_ACADEMICA = RestaurantViewModel(KEY_MENSA_ACADEMICA_PADERBORN, R.string.nameMensaAcademica)
        val MENSA_FORUM = RestaurantViewModel(KEY_MENSA_FORUM_PADERBORN, R.string.nameMensaForum)
        val BISTRO_HOTSPOT = RestaurantViewModel(KEY_BISTRO_HOTSPOT, R.string.nameBistroHotspot)
        val GRILL_CAFE = RestaurantViewModel(KEY_GRILL_CAFE, R.string.nameGrillCafe)
        val CAFETE = RestaurantViewModel(KEY_CAFETE, R.string.nameCafete)

        fun from(restaurant: Restaurant): RestaurantViewModel {
            return when (restaurant.key) {
                KEY_MENSA_ACADEMICA_PADERBORN -> MENSA_ACADEMICA
                KEY_MENSA_FORUM_PADERBORN -> MENSA_FORUM
                KEY_BISTRO_HOTSPOT -> BISTRO_HOTSPOT
                KEY_GRILL_CAFE -> GRILL_CAFE
                KEY_CAFETE -> CAFETE
                else -> RestaurantViewModel(restaurant.key, null)
            }
        }
    }
}