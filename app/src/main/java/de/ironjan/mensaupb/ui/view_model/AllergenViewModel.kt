package de.ironjan.mensaupb.ui.view_model

import de.ironjan.mensaupb.R
import de.ironjan.mensaupb.domain.Allergen
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_ANTIOXIDANTS
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_BLACKENED
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_CELERIAC
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_COFFEINE
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_COLORED
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_CONSERVED
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_CRUSTACEAN
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_EGGS
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_FISH
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_FLAVOR_ENHANCERS
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_GLUTEN
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_LACTOPROTEIN
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_LACTOSE
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_LUPINE
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_MOLLUSKS
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_MUSTARD
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_NITRATE_SALT
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_NUTS
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_PEANUTS
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_PHENYLALANINE
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_PHOSPHAT
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_QUININE
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_SESAME
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_SOYA
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_SULFITES
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_SULFURATED
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_SWEETENER
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_TAURINE
import de.ironjan.mensaupb.domain.Allergen.Companion.KEY_WAXED

// TODO: this is actually a view model class
data class AllergenViewModel(
    val key: String,
    val stringId: Int?
) {
    companion object Constants {

        val UNKNOWN = AllergenViewModel("", R.string.empty)
        val COLORED = AllergenViewModel(KEY_COLORED, R.string.colored)
        val CONSERVED = AllergenViewModel(KEY_CONSERVED, R.string.conserved)
        val ANTIOXIDANTS = AllergenViewModel(KEY_ANTIOXIDANTS, R.string.antioxidants)
        val FLAVOR_ENHANCERS = AllergenViewModel(KEY_FLAVOR_ENHANCERS, R.string.tasteEnhancer)
        val PHOSPHAT = AllergenViewModel(KEY_PHOSPHAT, R.string.phosphate)
        val SULFURATED = AllergenViewModel(KEY_SULFURATED, R.string.sulfured)
        val WAXED = AllergenViewModel(KEY_WAXED, R.string.waxed)
        val BLACKENED = AllergenViewModel(KEY_BLACKENED, R.string.blackened)
        val SWEETENER = AllergenViewModel(KEY_SWEETENER, R.string.sweetener)
        val PHENYLALANINE = AllergenViewModel(KEY_PHENYLALANINE, R.string.phenylalanine)
        val TAURINE = AllergenViewModel(KEY_TAURINE, R.string.taurine)
        val NITRATE_SALT = AllergenViewModel(KEY_NITRATE_SALT, R.string.nitrate_salt)
        val COFFEINE = AllergenViewModel(KEY_COFFEINE, R.string.caffeine)
        val QUININE = AllergenViewModel(KEY_QUININE, R.string.quinine)
        val LACTOPROTEIN = AllergenViewModel(KEY_LACTOPROTEIN, R.string.lacto_protein)
        val CRUSTACEAN = AllergenViewModel(KEY_CRUSTACEAN, R.string.crustacean)
        val EGGS = AllergenViewModel(KEY_EGGS, R.string.eggs)
        val FISH = AllergenViewModel(KEY_FISH, R.string.fish)
        val SOYA = AllergenViewModel(KEY_SOYA, R.string.soy)
        val LACTOSE = AllergenViewModel(KEY_LACTOSE, R.string.milk)
        val NUTS = AllergenViewModel(KEY_NUTS, R.string.nuts)
        val CELERIAC = AllergenViewModel(KEY_CELERIAC, R.string.celeriac)
        val MUSTARD = AllergenViewModel(KEY_MUSTARD, R.string.mustard)
        val SESAME = AllergenViewModel(KEY_SESAME, R.string.sesame)
        val SULFITES = AllergenViewModel(KEY_SULFITES, R.string.sulfates)
        val MOLLUSKS = AllergenViewModel(KEY_MOLLUSKS, R.string.mollusks)
        val LUPINE = AllergenViewModel(KEY_LUPINE, R.string.lupines)
        val GLUTEN = AllergenViewModel(KEY_GLUTEN, R.string.gluten)
        val PEANUTS = AllergenViewModel(KEY_PEANUTS, R.string.peanuts)

        fun from(allergen: Allergen): AllergenViewModel {
            return when (allergen.key) {
                KEY_COLORED -> COLORED
                KEY_CONSERVED -> CONSERVED
                KEY_ANTIOXIDANTS -> ANTIOXIDANTS
                KEY_FLAVOR_ENHANCERS -> FLAVOR_ENHANCERS
                KEY_PHOSPHAT -> PHOSPHAT
                KEY_SULFURATED -> SULFURATED
                KEY_WAXED -> WAXED
                KEY_BLACKENED -> BLACKENED
                KEY_SWEETENER -> SWEETENER
                KEY_PHENYLALANINE -> PHENYLALANINE
                KEY_TAURINE -> TAURINE
                KEY_NITRATE_SALT -> NITRATE_SALT
                KEY_COFFEINE -> COFFEINE
                KEY_QUININE -> QUININE
                KEY_LACTOPROTEIN -> LACTOPROTEIN
                KEY_CRUSTACEAN -> CRUSTACEAN
                KEY_EGGS -> EGGS
                KEY_FISH -> FISH
                KEY_SOYA -> SOYA
                KEY_LACTOSE -> LACTOSE
                KEY_NUTS -> NUTS
                KEY_CELERIAC -> CELERIAC
                KEY_MUSTARD -> MUSTARD
                KEY_SESAME -> SESAME
                KEY_SULFITES -> SULFITES
                KEY_MOLLUSKS -> MOLLUSKS
                KEY_LUPINE -> LUPINE
                KEY_GLUTEN -> GLUTEN
                KEY_PEANUTS -> PEANUTS
                else -> AllergenViewModel(allergen.key, null)
            }
        }
    }
}

