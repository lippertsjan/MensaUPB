package de.ironjan.mensaupb.ui.view_model

data class MenuListItemViewModel(
    val name: String,
    val price: Double,
    val badges: List<BadgeViewModel>,
    val allergens: List<AllergenViewModel>
)