package de.ironjan.mensaupb.application.ports.out.persistence

import de.ironjan.mensaupb.domain.Menu

// TODO: draft. api needs to be cleaned up...
interface MenusRepository {
    fun save(menu: Menu);
}