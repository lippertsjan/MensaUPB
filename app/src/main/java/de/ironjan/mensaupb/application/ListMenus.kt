package de.ironjan.mensaupb.application

import de.ironjan.mensaupb.domain.Menu

interface ListMenus {
    fun getMenus() : List<Menu>;
}