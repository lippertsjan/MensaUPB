package de.ironjan.mensaupb.application.ports.out.network

import android.annotation.SuppressLint
import de.ironjan.mensaupb.domain.Allergen
import de.ironjan.mensaupb.domain.Badge
import de.ironjan.mensaupb.domain.Menu
import java.text.SimpleDateFormat

data class MenuDto(
    val date: String,
    val name_de: String,
    val name_en: String,
    val description_de: String,
    val description_en: String,
    val category: String,
    val category_de: String,
    val category_en: String,
    val subcategory_de: String,
    val subcategory_en: String,
    val priceStudents: Double,
    val priceWorkers: Double,
    val priceGuests: Double,
    val allergens: Array<String>,
    val order_info: Int,
    val badges: Array<String>,
    val restaurant: String,
    val pricetype: String,
    val image: String,
    val key: String
) {

    fun toMenu(): Menu {
        return toMenu(this)
    }

    companion object {
        @SuppressLint("SimpleDateFormat")
        val DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd")
        fun toMenu(menuDto: MenuDto): Menu {
            return Menu(
                DATE_FORMAT.parse(menuDto.date)!!,
                menuDto.name_de,
                menuDto.name_en,
                menuDto.description_de,
                menuDto.description_en,
                menuDto.category,
                menuDto.category_de,
                menuDto.category_en,
                menuDto.subcategory_de,
                menuDto.subcategory_en,
                menuDto.priceStudents,
                menuDto.priceWorkers,
                menuDto.priceGuests,
                menuDto.allergens.map { Allergen(it) },
                menuDto.order_info,
                menuDto.badges.map { Badge(it) },
                menuDto.restaurant,
                menuDto.pricetype,
                menuDto.image,
                menuDto.key
            )
        }
    }
}