package de.ironjan.mensaupb.application.ports.out.network

import de.ironjan.mensaupb.domain.Menu

interface MenusApi {
    fun getMenus(): List<Menu>?
}