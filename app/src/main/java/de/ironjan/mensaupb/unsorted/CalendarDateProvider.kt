//// TODO sort into correct packages
package de.ironjan.mensaupb.unsorted

import java.util.Calendar
import java.util.Date

class CalendarDateProvider : DateProvider {
    override fun today(): Date {
        return Calendar.getInstance().time
    }
}