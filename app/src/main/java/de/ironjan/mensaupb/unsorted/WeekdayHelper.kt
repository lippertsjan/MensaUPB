package de.ironjan.mensaupb.unsorted

import android.content.Context
import de.ironjan.mensaupb.R
import java.lang.IllegalArgumentException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date

class WeekdayHelper(val context: Context, val dateProvider: DateProvider) {

    fun getNextWeekDayForUI(i: Int): String?{
        // TODO: requires test
        if (weekDaysforUi.get(i) == null){
            val localizedDatePattern = context.getString(R.string.localizedDatePattern)
            val simpleDateFormat = SimpleDateFormat(localizedDatePattern)
            weekDaysforUi[i] = simpleDateFormat.format(getNextWeekDayWithOffset(i))
        }
        return weekDaysforUi.get(i)
    }

    @Synchronized
    fun getNextWeekDayAsKey(i: Int): String? {
        // TODO: requires test
        // TODO: is synchronized necessary?
        if (weekDaysAsString.get(i) == null) {
            weekDaysAsString[i] = SDF.format(getNextWeekDayWithOffset(i))
        }
        return weekDaysAsString.get(i)
    }

    /**
     * Returns the next weekday with offset from today. Does not handle more than 1 week.
     * TODO: perhaps change logic? Filter non-weekdays out "upstairs"?
     */
    fun getNextWeekDayWithOffset(offset: Int): Date {
        if (offset>7) throw IllegalArgumentException("does not handle offset > 7")

        val today = dateProvider.today()
        val calendar = Calendar.getInstance()
        calendar.time = today

        val todayDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)
        calendar.add(Calendar.DAY_OF_WEEK, offset)
        calendar.add(Calendar.DAY_OF_WEEK, computeAdditionalOffset(todayDayOfWeek, offset))
        return calendar.time
    }

    /**
     * Computes the additional offset to skip the weekend.
     * `day, offset  |  0 |  1 |  2 |  3 |  4 |
     * monday       |    |    |    |    |    |
     * tuesday      |    |    |    |    | +2 |
     * wednesday    |    |    |    | +2 | +2 |
     * thursday     |    |    | +2 | +2 | +2 |
     * friday       |    | +2 | +2 | +2 | +2 |
     * saturday     | +2 | +2 | +2 | +2 | +2 |
     * sunday       | +1 | +1 | +1 | +1 | +1 |
    ` *
     *
     * @param today the day which is "today"
     * @param offset    offset applied to "today"
     * @return the additional offset
     */
    private fun computeAdditionalOffset(today: Int, offset: Int): Int {
        return when (today) {
            Calendar.MONDAY -> 0
            Calendar.TUESDAY -> if (offset >= 4) 2 else 0
            Calendar.WEDNESDAY -> if (offset >= 3) 2 else 0
            Calendar.THURSDAY -> if (offset >= 2) 2 else 0
            Calendar.FRIDAY -> if (offset >= 1) 2 else 0
            Calendar.SATURDAY -> 2
            Calendar.SUNDAY -> 1
            else -> 0
        }
    }

    companion object {
        const val DISPLAYED_DAYS_COUNT = 5
        private const val CACHED_DAYS_COUNT = DISPLAYED_DAYS_COUNT + 2
        private val SDF = SimpleDateFormat("yyyy-MM-dd")
        private val weekDaysAsString = arrayOfNulls<String>(CACHED_DAYS_COUNT)
        private val weekDaysforUi = arrayOfNulls<String>(CACHED_DAYS_COUNT)
    }
}