package de.ironjan.mensaupb.unsorted

import java.util.Date

interface DateProvider {
    fun today(): Date
}