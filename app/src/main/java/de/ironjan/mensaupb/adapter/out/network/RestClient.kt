package de.ironjan.mensaupb.adapter.out.network

interface RestClient {
    /// Sends a get request to the given url and returns the body
    fun get(url: String) : String;
}