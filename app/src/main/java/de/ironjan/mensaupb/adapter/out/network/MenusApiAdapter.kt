package de.ironjan.mensaupb.adapter.out.network

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import de.ironjan.mensaupb.application.ports.out.network.MenuDto
import de.ironjan.mensaupb.application.ports.out.network.MenusApi
import de.ironjan.mensaupb.domain.Menu

class MenusApiAdapter(private val restClient: RestClient) : MenusApi {

    override fun getMenus(): List<Menu>? {
        val response = restClient.get(MENUS_PATH)

        val type = object : TypeToken<Array<MenuDto>>() {}.type
        val dtos = Gson().fromJson<Array<MenuDto>>(response, type)
        return dtos?.map { MenuDto.toMenu(it) }
    }

    companion object {
        const val API_BASE_PATH = "https://mensaupb.herokuapp.com/api"
        const val MENUS_PATH = API_BASE_PATH + "/menus"
    }
}
