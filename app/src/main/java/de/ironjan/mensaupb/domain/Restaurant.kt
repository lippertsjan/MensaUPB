package de.ironjan.mensaupb.domain

data class Restaurant(val key: String) {

    companion object {
        val KEY_MENSA_ACADEMICA_PADERBORN = "mensa-academica-paderborn"
        val KEY_MENSA_FORUM_PADERBORN = "mensa-forum-paderborn"
        val KEY_BISTRO_HOTSPOT = "bistro-hotspot"
        val KEY_GRILL_CAFE = "grill-cafe"
        val KEY_CAFETE = "cafete"
    }

}