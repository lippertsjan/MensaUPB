package de.ironjan.mensaupb.domain

data class Allergen(val key: String) {
    companion object {

        val KEY_COLORED = "1"
        val KEY_CONSERVED = "2"
        val KEY_ANTIOXIDANTS = "3"
        val KEY_FLAVOR_ENHANCERS = "4"
        val KEY_PHOSPHAT = "5"
        val KEY_SULFURATED = "6"
        val KEY_WAXED = "7"
        val KEY_BLACKENED = "8"
        val KEY_SWEETENER = "9"
        val KEY_PHENYLALANINE = "10"
        val KEY_TAURINE = "11"
        val KEY_NITRATE_SALT = "12"
        val KEY_COFFEINE = "13"
        val KEY_QUININE = "14"
        val KEY_LACTOPROTEIN = "15"
        val KEY_CRUSTACEAN = "A2"
        val KEY_EGGS = "A3"
        val KEY_FISH = "A4"
        val KEY_SOYA = "A6"
        val KEY_LACTOSE = "A7"
        val KEY_NUTS = "A8"
        val KEY_CELERIAC = "A9"
        val KEY_MUSTARD = "A10"
        val KEY_SESAME = "A11"
        val KEY_SULFITES = "A12"
        val KEY_LUPINE = "A13"
        val KEY_MOLLUSKS = "A14"
        val KEY_GLUTEN = "A1"
        val KEY_PEANUTS = "A5"
    }
}
