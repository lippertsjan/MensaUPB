package de.ironjan.mensaupb.domain

import java.util.Date

data class Menu(
    val date: Date,
    val name_de: String,
    val name_en: String,
    val description_de: String,
    val description_en: String,
    val category: String,
    val category_de: String,
    val category_en: String,
    val subcategory_de: String,
    val subcategory_en: String,
    val priceStudents: Double,
    val priceWorkers: Double,
    val priceGuests: Double,
    val allergens: List<Allergen>,
    val order_info: Int,
    val badges: List<Badge>,
    val restaurant: String,
    val pricetype: String,
    val image: String,
    val key: String
)


