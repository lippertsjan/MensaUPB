package de.ironjan.mensaupb.domain

data class Badge(val key: String) {
    companion object {
        const val KEY_LOW_CALORIE = "low-calorie"
        const val KEY_FAT_FREE = "nonfat"
        const val KEY_VEGETARIAN = "vegetarian"
        const val KEY_VEGAN = "vegan"
        const val KEY_NO_LACTOSE = "lactose-free"
        const val KEY_NO_GLUTEN = "gluten-free"
    }
}