 * Fixed: Wochenende wird nun korrekt übersprungen. 
 * Fixed: Fehler beim Betrachten einzelner Gerichte behoben. 
 * Fixed: Einer der Mitwirkenden hat in "Über die App" gefehlt
 * Added: Schnelleres Laden der Gerichte.
